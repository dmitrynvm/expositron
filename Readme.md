# Human motion recognition and intention prediction
## Command line utility for data pipeline automation

1. Installation
```
conda create -n expose python=3.9
git clone https://gitlab.com/dmitrynvm/expositron
cd expositron
pip3 install -U pip setuptools 
python3 -m venv venv
source venv/bin/activate
alias expositron="python3 -m expositron"
```
cd expositron
pip3 install -e .

0. Install
```
conda create -n cv python=3.9 anaconda
conda activate cv
cd resources/tools
pip install PyOpenGL-3.1.6-cp39-cp39-win_amd64.whl
pip install PyOpenGL_accelerate-3.1.6-cp39-cp39-win_amd64.whl
conda install pytorch torchvision torchaudio cudatoolkit=11.3 -c pytorch
mkdir pipeline
mv $HOME/Downloads/resources.zip pipeline
cd pipeline; unzip resources.zip
```

1. Prepare
```
expositron prepare SOURCE DEST [FMT]
```

2. Parse
```
expositron parse SOURCE DEST
```
