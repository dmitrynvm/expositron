import setuptools
from setuptools import setup


def parse(file):
    with open(file) as f:
        return [l.strip('\n') for l in f if l.strip('\n') and not l.startswith('#')]


setuptools.setup(
    name='expositron',
    version='0.1.14',
    author='dmitrynvm',
    author_email='dmitrynvm@gmail.com',
    description='A Toolkit for Deep Learning in Gesture recognition and intention prediction',
    long_description='',
    long_description_content_type='text/markdown',
    url='https://www.gitlab.com/dmitrynvm/expositron',
    keywords=['Deep Learning', 'Gesture recognition', 'Intention prediction', 'AI'],
    packages=setuptools.find_packages(exclude=('tests',)),
    package_data={},
    install_requires=parse('requirements.txt'),
    extras_require={},
    classifiers=[
        'Programming Language :: Python :: 3.9',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.6',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
    ],
)