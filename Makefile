install:
	pip3 install -U pip setuptools 
	pip install -e .

parse:
	python3 main.py parse input output

demo:
	resources/tools/blender/blender.exe --python scripts/arma.py

skeleton:
	resources/tools/blender/blender.exe --python resources/scripts/skeleton.py

clean:
	rm -rf build gradle gradlew*

publish:
	scp build/distributions/nous*.zip ${name}@188.124.50.201:/home/${name}
