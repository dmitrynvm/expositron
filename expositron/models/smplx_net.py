import torch.nn as nn
from expositron.models.attention.predictor import SMPLXHead


class SMPLXNet(nn.Module):

    def __init__(self, config):
        super(SMPLXNet, self).__init__()

        self.config = config.clone()
        network_cfg = config.get('network', {})
        self.net_type = network_cfg.get('type', 'attention')
        self.smplx = SMPLXHead(config)

    def toggle_losses(self, iteration):
        self.smplx.toggle_losses(iteration)

    def get_hand_model(self) -> nn.Module:
        return self.smplx.get_hand_model()

    def get_head_model(self) -> nn.Module:
        return self.smplx.get_head_model()

    def forward(
        self,
        images,
        targets,
        hand_imgs=None,
        hand_targets=None,
        head_imgs=None,
        head_targets=None,
        full_imgs=None,
        device=None
    ):
        return self.smplx(
            images,
            targets=targets,
            hand_imgs=hand_imgs,
            hand_targets=hand_targets,
            head_imgs=head_imgs,
            head_targets=head_targets,
            full_imgs=full_imgs
        )
