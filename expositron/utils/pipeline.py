import functools
import numpy as np
import os
import torch

from collections import defaultdict
from torchvision.models.detection import keypointrcnn_resnet50_fpn
from torchvision.transforms import Compose, ToTensor
from expositron.utils.image import ImageFolder, ImageFolderWithBoxes
from expositron.data.build import collate_batch
from expositron.data.transforms import build_transforms
from expositron.utils.io import get_device


def collate_fn(batch):
    output_dict = dict()
    for d in batch:
        for key, val in d.items():
            if key not in output_dict:
                output_dict[key] = []
            output_dict[key].append(val)
    return output_dict


def load_data(
    source: str,
    config,
    num_workers: int = 8,
    min_score: float = 0.5,
    scale_factor: float = 1.2,
    device: torch.device = get_device()
) -> torch.utils.data.DataLoader:
    rcnn_model = keypointrcnn_resnet50_fpn(pretrained=True)
    rcnn_model.eval()
    rcnn_model = rcnn_model.to(device=device)
    transform = Compose([ToTensor(), ])

    dataset = ImageFolder(source, transforms=transform)
    rcnn_dloader = torch.utils.data.DataLoader(
        dataset,
        num_workers=num_workers,
        collate_fn=collate_fn
    )

    img_paths = []
    bboxes = []
    for bidx, batch in enumerate(rcnn_dloader):
        batch['images'] = [x.to(device=device) for x in batch['images']]

        output = rcnn_model(batch['images'])
        for ii, x in enumerate(output):
            img = np.transpose(batch['images'][ii].detach().cpu().numpy(), [1, 2, 0])
            img = (img * 255).astype(np.uint8)
            img_path = batch['paths'][ii]
            _, fname = os.path.split(img_path)
            fname, _ = os.path.splitext(fname)

            for n, bbox in enumerate(output[ii]['boxes']):
                bbox = bbox.detach().cpu().numpy()
                if output[ii]['scores'][n].item() < min_score:
                    continue
                img_paths.append(img_path)
                bboxes.append(bbox)

    dataset_cfg = config.get('datasets', {})
    body_dsets_cfg = dataset_cfg.get('body', {})

    body_transfs_cfg = body_dsets_cfg.get('transforms', {})
    transforms = build_transforms(body_transfs_cfg, is_train=False)
    batch_size = body_dsets_cfg.get('batch_size', 64)
    dataset = ImageFolderWithBoxes(
        img_paths,
        bboxes,
        scale_factor=scale_factor,
        transforms=transforms
    )
    collater = functools.partial(
        collate_batch,
        use_shared_memory=num_workers > 0,
        return_full_imgs=True
    )
    dataloader = torch.utils.data.DataLoader(
        dataset,
        batch_size=batch_size,
        num_workers=num_workers,
        collate_fn=collater,
        drop_last=False,
        pin_memory=True,
    )
    return dataloader


def weak_persp_to_blender(
        targets,
        camera_scale,
        camera_transl,
        H,
        W,
        sensor_width=36,
        focal_length=5000
):
    if torch.is_tensor(camera_scale):
        camera_scale = camera_scale.detach().cpu().numpy()
    if torch.is_tensor(camera_transl):
        camera_transl = camera_transl.detach().cpu().numpy()

    output = defaultdict(lambda: [])
    for ii, target in enumerate(targets):
        orig_bbox_size = target.get_field('orig_bbox_size')
        bbox_center = target.get_field('orig_center')
        z = 2 * focal_length / (camera_scale[ii] * orig_bbox_size)
        transl = [
            camera_transl[ii, 0].item(),
            camera_transl[ii, 1].item(),
            z.item()
        ]
        shift_x = - (bbox_center[0] / W - 0.5)
        shift_y = (bbox_center[1] - 0.5 * H) / W
        focal_length_in_mm = focal_length / W * sensor_width
        output['shift_x'].append(shift_x)
        output['shift_y'].append(shift_y)
        output['transl'].append(transl)
        output['focal_length_in_mm'].append(focal_length_in_mm)
        output['focal_length_in_px'].append(focal_length)
        output['center'].append(bbox_center)
        output['sensor_width'].append(sensor_width)
    for key in output:
        output[key] = np.stack(output[key], axis=0)
    return output


def denormalize(image, mean, std, add_alpha=True):
    if torch.is_tensor(image):
        image = image.detach().cpu().numpy().squeeze()

    out_img = (image * std[np.newaxis, :, np.newaxis, np.newaxis] +
               mean[np.newaxis, :, np.newaxis, np.newaxis])
    if add_alpha:
        out_img = np.pad(
            out_img, [[0, 0], [0, 1], [0, 0], [0, 0]],
            mode='constant', constant_values=1.0)
    return out_img
