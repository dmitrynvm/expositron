import numpy as np
import torch
from typing import NewType

Tensor = NewType('Tensor', torch.Tensor)
Array = NewType('Array', np.ndarray)
