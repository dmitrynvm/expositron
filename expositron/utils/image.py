import cv2
import os
import os.path as osp
import numpy as np
import torch.utils.data as dutils
from expositron.data.utils import bbox_to_center_scale
from expositron.data.targets import BoundingBox
from expositron.utils.types import Array

EXTS = ['.jpg', '.jpeg', '.png']


def read_img(img_fn: str, dtype=np.float32) -> Array:
    img = cv2.cvtColor(cv2.imread(img_fn), cv2.COLOR_BGR2RGB)
    if dtype == np.float32:
        if img.dtype == np.uint8:
            img = img.astype(dtype) / 255.0
    return img


class ImageFolder(dutils.Dataset):
    def __init__(self,
                 data_folder='resources/images',
                 transforms=None,
                 **kwargs):
        super(ImageFolder, self).__init__()
        paths = []
        self.transforms = transforms
        data_folder = osp.expandvars(data_folder)
        for fname in sorted(os.listdir(data_folder)):
            if not any(fname.endswith(ext) for ext in EXTS):
                continue
            paths.append(osp.join(data_folder, fname))
        self.paths = np.stack(paths)

    def __len__(self):
        return len(self.paths)

    def __getitem__(self, index):
        img = read_img(self.paths[index])
        if self.transforms is not None:
            img = self.transforms(img)
        return {
            'images': img,
            'paths': self.paths[index]
        }


class ImageFolderWithBoxes(dutils.Dataset):
    def __init__(self,
                 img_paths,
                 bboxes,
                 transforms=None,
                 scale_factor=1.2,
                 **kwargs):
        super(ImageFolderWithBoxes, self).__init__()

        self.transforms = transforms

        self.paths = np.stack(img_paths)
        self.bboxes = np.stack(bboxes)
        self.scale_factor = scale_factor

    def __len__(self):
        return len(self.paths)

    def __getitem__(self, index):
        img = read_img(self.paths[index])
        bbox = self.bboxes[index]
        target = BoundingBox(bbox, size=img.shape)
        center, scale, bbox_size = bbox_to_center_scale(bbox, dset_scale_factor=self.scale_factor)
        target.add_field('bbox_size', bbox_size)
        target.add_field('orig_bbox_size', bbox_size)
        target.add_field('orig_center', center)
        target.add_field('center', center)
        target.add_field('scale', scale)

        _, fname = osp.split(self.paths[index])
        target.add_field('fname', f'{fname}_{index:03d}')
        target.add_field('fname', f'{fname}')

        if self.transforms is not None:
            full_img, cropped_image, target = self.transforms(img, target)

        return full_img, cropped_image, target, index
