import os
import os.path as osp
import shutil
import torch
from pathlib import Path


def rm(folder: str):
    if os.path.exists(folder):
        if osp.isfile(folder):
            os.remove(folder)
        else:
            shutil.rmtree(folder)


def copy(src, dst):
    if os.path.isfile(src):
        shutil.copy(src, dst)
    else:
        for src_folder, _, files in os.walk(src):
            dst_folder = src_folder.replace(src, dst)
            touch(dst_folder)
            for f in files:
                file = os.path.join(src_folder, f)
                shutil.copy(file, dst_folder)


def move(src, dst):
    if os.path.isfile(src):
        shutil.move(src, dst)
    else:
        for src_folder, _, files in os.walk(src):
            dst_folder = src_folder.replace(src, dst)
            touch(dst_folder)
            for f in files:
                file = os.path.join(src_folder, f)
                shutil.move(file, dst_folder)


def touch(folder: str):
    os.makedirs(folder, exist_ok=True)


def get_device():
    return torch.device('cuda') if torch.cuda.is_available else torch.device('cpu')


def append(folder, filename, text):
    with open(osp.join(folder, filename), 'a') as f:
        f.write(text + '\n')


def write(folder, filename, text):
    with open(osp.join(folder, filename), 'w') as f:
        f.write(text + '\n')


def name_lens(inp):
    return inp.name


def list_files(inp, ext=None):
    return [(f.name, f.path) for f in sorted(os.scandir(inp), key=name_lens) if f.is_file() and f.name.endswith(ext)]


def enum_files(inp, ext=None):
    return [(i, osp.basename(f.name), f.path) for i, f in enumerate(sorted(os.scandir(inp), key=name_lens)) if f.is_file() and f.name.endswith(ext)]


def list_folders(inp):
    return [(f.name, f.path) for f in sorted(os.scandir(inp), key=name_lens) if f.is_dir()]


def enum_folders(inp):
    return [(i, f.name, f.path) for i, f in enumerate(sorted(os.scandir(inp), key=name_lens)) if f.is_dir()]


def get_files(folder, ext=None):
    outputs = []
    if type(ext) is list:
        for root, folder, files in sorted(os.walk(folder)):
            output = []
            for file in sorted(files):
                for e in ext:
                    if file.endswith(e):
                        output += [osp.join(root, file)]
            if output:
                outputs += [output]
    else:
        for root, folder, files in sorted(os.walk(folder)):
            for file in sorted(files):
                if ext == None:
                    outputs += [osp.join(root, file)]
                else:
                    if file.endswith(ext):
                        outputs += [osp.join(root, file)]
    return outputs


def enumerate_files(folder, ext):
    outputs = []
    for i, (csv_file, json_file) in enumerate(get_files(folder, ext)):
        outputs += [(i, csv_file, json_file)]
    return outputs


def split(line, n):
    return [line[i:i+n] for i in range(0, len(line), n)]
