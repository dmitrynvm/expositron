import click
from expositron.config import dft
from expositron.stages import *


@click.group()
def app():
    pass


@app.command()
@click.argument('src')
@click.argument('dst')
def load(src, dst):
    load_stage(src, dst)


@app.command()
@click.argument('src')
def check(src):
    check_stage(src)


@app.command()
@click.argument('src')
def clean(src):
    clean_stage(src)


@app.command()
@click.argument('src')
@click.argument('dst')
@click.option('-c', '--cfg', default='resources/config.yml')
def extract(src, dst, cfg):
    dft.merge_from_file(cfg)
    extract_stage(src, dst, dft)


@app.command()
@click.argument('src')
@click.argument('dst')
def media(src, dst):
    media_stage(src, dst)
