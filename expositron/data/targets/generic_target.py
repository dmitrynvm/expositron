from abc import ABC


class GenericTarget(ABC):
    def __init__(self):
        super(GenericTarget, self).__init__()
        self.extra_fields = {}

    def __del__(self):
        if hasattr(self, 'extra_fields'):
            self.extra_fields.clear()

    def add_field(self, field, field_data):
        self.extra_fields[field] = field_data

    def get_field(self, field):
        return self.extra_fields[field]

    def has_field(self, field):
        return field in self.extra_fields

    def delete_field(self, field):
        if field in self.extra_fields:
            del self.extra_fields[field]

    def transpose(self, method):
        for k, v in self.extra_fields.items():
            if isinstance(v, GenericTarget):
                v = v.transpose(method)
            self.add_field(k, v)
        self.add_field('is_flipped', True)
        return self

    def rotate(self, *args, **kwargs):
        for k, v in self.extra_fields.items():
            if isinstance(v, GenericTarget):
                v = v.rotate(*args, **kwargs)
        self.add_field('rot', kwargs.get('rot', 0))
        return self

    def crop(self, *args, **kwargs):
        for k, v in self.extra_fields.items():
            if isinstance(v, GenericTarget):
                v = v.crop(*args, **kwargs)
        return self

    def resize(self, *args, **kwargs):
        for k, v in self.extra_fields.items():
            if isinstance(v, GenericTarget):
                v = v.resize(*args, **kwargs)
            self.add_field(k, v)
        return self

    def to_tensor(self, *args, **kwargs):
        for k, v in self.extra_fields.items():
            if isinstance(v, GenericTarget):
                v.to_tensor(*args, **kwargs)
            self.add_field(k, v)

    def to(self, *args, **kwargs):
        for k, v in self.extra_fields.items():
            if hasattr(v, "to"):
                v = v.to(*args, **kwargs)
        return self
