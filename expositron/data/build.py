from typing import NewType, List, Tuple, Union
import torch
import torch.utils.data as dutils
from .utils import EqualSampler
from .targets.generic_target import GenericTarget
from .targets.image_list import to_image_list, ImageList

from expositron.utils.types import Tensor

DEFAULT_NUM_WORKERS = {
    'train': 0,
    'val': 0,
    'test': 0
}


class MemoryPinning(object):
    def __init__(
        self,
        full_img_list: Union[ImageList, List[Tensor]],
        images: Tensor,
        targets: List[GenericTarget]
    ):
        super(MemoryPinning, self).__init__()
        self.img_list = full_img_list
        self.images = images
        self.targets = targets

    def pin_memory(
            self
    ) -> Tuple[Union[ImageList, List[Tensor]], Tensor, List[GenericTarget]]:
        if self.img_list is not None:
            if isinstance(self.img_list, ImageList):
                self.img_list.pin_memory()
            elif isinstance(self.img_list, (list, tuple)):
                self.img_list = [x.pin_memory() for x in self.img_list]
        return (
            self.img_list,
            self.images.pin_memory(),
            self.targets,
        )


def collate_batch(batch, use_shared_memory=False, return_full_imgs=False,
                  pin_memory=True):
    if return_full_imgs:
        images, cropped_images, targets, _ = zip(*batch)
    else:
        _, cropped_images, targets, _ = zip(*batch)

    out_targets = []
    for t in targets:
        if t is None:
            continue
        if type(t) == list:
            out_targets += t
        else:
            out_targets.append(t)
    out_cropped_images = []
    for img in cropped_images:
        if img is None:
            continue
        if len(img.shape) < 4:
            img.unsqueeze_(dim=0)
        out_cropped_images.append(img.clone())

    if len(out_cropped_images) < 1:
        return None, None, None

    full_img_list = None
    if return_full_imgs:
        #  full_img_list = to_image_list(images)
        full_img_list = images
    out = None
    if use_shared_memory:
        numel = sum([x.numel() for x in out_cropped_images if x is not None])
        storage = out_cropped_images[0].storage()._new_shared(numel)
        out = out_cropped_images[0].new(storage)

    #  if not return_full_imgs:
        #  del images
        #  images = None

    batch.clear()
    #  del targets, batch
    if pin_memory:
        return MemoryPinning(
            full_img_list,
            torch.cat(out_cropped_images, 0, out=out),
            out_targets
        )
    else:
        return full_img_list, torch.cat(
            out_cropped_images, 0, out=out), out_targets


def make_equal_sampler(datasets, batch_size=32, shuffle=True, ratio_2d=0.5):
    batch_sampler = EqualSampler(
        datasets, batch_size=batch_size, shuffle=shuffle, ratio_2d=ratio_2d)
    out_dsets_lst = [dutils.ConcatDataset(datasets) if len(datasets) > 1 else
                     datasets[0]]
    return batch_sampler, out_dsets_lst

