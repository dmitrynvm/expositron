import torch
import torch.nn as nn


def build_robustifier(robustifier_type: str = None, **kwargs) -> nn.Module:
    if robustifier_type is None or robustifier_type == 'none':
        return None
    elif robustifier_type == 'gmof':
        return GMOF(**kwargs)
    else:
        raise ValueError(f'Unknown robustifier: {robustifier_type}')


class GMOF(nn.Module):
    def __init__(self, rho: float = 100, **kwargs) -> None:
        super(GMOF, self).__init__()
        self.rho = rho

    def extra_repr(self):
        return f'Rho = {self.rho}'

    def forward(self, residual):
        squared_residual = residual.pow(2)
        return torch.div(squared_residual, squared_residual + self.rho ** 2)
