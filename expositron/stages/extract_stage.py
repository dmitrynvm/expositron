import json
import os
import torch
import warnings
import open3d as o3d
import os.path as osp
import pandas as pd
from alive_progress import alive_bar
from scipy import signal
from expositron.data.targets.image_list import to_image_list
from expositron.data.targets.keypoints import *
from expositron.models.smplx_net import SMPLXNet
# from expositron.utils.draw import HDRenderer
from expositron.utils.pipeline import *
from expositron.utils.rotation import *
from expositron.utils import *
warnings.filterwarnings('ignore')
os.environ['CUDA_LAUNCH_BLOCKING'] = '1'


@torch.no_grad()
def extract_stage(
    src,
    dst,
    cfg,
    save_image=False,
    save_stage=False,
    save_final=False,
    save_mesh=True
) -> None:
    os.makedirs(dst, exist_ok=True)
    device = get_device()
    model = SMPLXNet(cfg)
    model = model.to(device=device)
    model.eval()
    data = torch.load('resources/checkpoints/model.ckpt', map_location=get_device())
    model.load_state_dict(data['model'], strict=False)
    model = model.eval()
    means = np.array(cfg.datasets.body.transforms.mean)
    std = np.array(cfg.datasets.body.transforms.std)
    # renderer = HDRenderer(img_size=2560)
    src_folders = list_folders(src)

    joint_cols = []
    for name in NTU25_KEYPOINTS:
        joint_cols += [f'{name}__x']
        joint_cols += [f'{name}__y']
        joint_cols += [f'{name}__z']

    with alive_bar(len(src_folders), theme='classic') as bar:
        for folder, src_folder in src_folders:
            bar()
            joint_rows = []
            # pose_rows = []
            loaded = load_data(src_folder, cfg, device=device)

            for bi, batch in enumerate(loaded):
                full_imgs_list, body_imgs, body_targets = batch
                full_imgs = to_image_list(full_imgs_list)
                body_imgs = body_imgs.to(device=device)
                body_targets = [target.to(device) for target in body_targets]
                full_imgs = full_imgs.to(device=device)
                torch.cuda.synchronize()

                model_output = model( body_imgs, body_targets, full_imgs=full_imgs, device=device)
                torch.cuda.synchronize()
                images = full_imgs.images.detach().cpu().numpy().squeeze()

                _, _, h, w = full_imgs.shape
                images = np.clip(np.transpose(denormalize(images, means, std), [0, 2, 3, 1]), 0, 1.0)
                body_output = model_output['body']

                # n_stages = body_output['num_stages']
                stage_output = body_output[f'stage_02']
                faces = stage_output['faces']
                stage_vertices = stage_output['vertices']
                final_output = model_output['body']['final']

                joints = stage_output['joints'].detach().cpu().numpy()
                vertices = stage_output['vertices'].detach().cpu().numpy()
                poses = stage_output['full_pose'].detach().cpu().numpy()
                camera_parameters = model_output['body']['camera_parameters']
                camera_scale = camera_parameters['scale'].detach()
                camera_transl = camera_parameters['translation'].detach()

                hd_params = weak_persp_to_blender(
                    body_targets,
                    camera_scale=camera_scale,
                    camera_transl=camera_transl,
                    H=h,
                    W=w,
                    sensor_width=36,
                    focal_length=5000
                )
                # bg_images = np.transpose(images, [0, 3, 1, 2])
                # stage_overlay = renderer(
                #     stage_vertices,
                #     faces,
                #     focal_length=hd_params['focal_length_in_px'],
                #     camera_translation=hd_params['transl'],
                #     camera_center=hd_params['center'],
                #     bg_imgs=bg_images,
                #     return_with_alpha=True,
                #     # body_color=[0.4, 0.4, 0.7]
                #     body_color=[1.0, 0.75, 0.8]
                # )
                # stage_overlay = np.clip(np.transpose(stage_overlay, [0, 2, 3, 1]) * 255, 0, 255).astype(np.uint8)
                # final_overlay = renderer(
                #     vertices,
                #     faces,
                #     focal_length=hd_params['focal_length_in_px'],
                #     camera_translation=hd_params['transl'],
                #     camera_center=hd_params['center'],
                #     bg_imgs=bg_images,
                #     return_with_alpha=True,
                #     body_color=[0.4, 0.4, 0.7]
                # )
                # final_overlay = np.clip(np.transpose(final_overlay, [0, 2, 3, 1]) * 255, 0, 255).astype(np.uint8)
                for i, body_target in enumerate(body_targets):
                    mesh = o3d.geometry.TriangleMesh()
                    mesh.vertices = o3d.utility.Vector3dVector(vertices[i])
                    mesh.triangles = o3d.utility.Vector3iVector(faces)
                    rotmat = mesh.get_rotation_matrix_from_xyz((np.pi , 0, 0))
                    mesh.rotate(rotmat, center=(0, 0, 0))
                    bottom = np.min(np.asarray(mesh.vertices)[:, 2])
                    mesh.translate((0, 0,-bottom))

                    # if save_image:
                    #     img_file = body_target.get_field('fname')
                    #     os.makedirs(osp.join(dst, folder + '_origin'), exist_ok=True)
                    #     shutil.copy(osp.join(src_folder, img_file), osp.join(dst, folder + '_origin', f'image{i+1:03d}.jpg'))

                    # if save_stage:
                    #     os.makedirs(osp.join(dst, folder + '_stage'), exist_ok=True)
                    #     Image.fromarray(stage_overlay[i]).save(os.path.join(dst, folder + '_stage', f'image{i+1:03d}.png'))

                    # if save_final:
                    #     os.makedirs(osp.join(dst, folder + '_final'), exist_ok=True)
                    #     Image.fromarray(final_overlay[i]).save(os.path.join(dst, folder + '_final', f'image{i+1:03d}.png'))

                    # if save_mesh:
                    #     os.makedirs(dst + '_model', exist_ok=True)
                    #     os.makedirs(osp.join(dst + '_model', folder), exist_ok=True)
                    #     o3d.io.write_triangle_mesh(os.path.join(dst + '_model', folder, f'frame{i+1:03d}.ply'), mesh)

                    n2j = {}
                    for name, joint in zip(KEYPOINT_NAMES, joints[i]):
                        if name in NTU25_KEYPOINTS:
                            n2j[name] = joint.tolist()
                            # n2j[name] = (np.dot(rotmat, np.asarray(joint.tolist())) + np.array((0, 0, -bottom))).tolist()

                    joint_row = []
                    for name in NTU25_KEYPOINTS:
                        joint_row += n2j[name]
                    joint_rows += [joint_row]
                    # s = poses[i].shape
                    # pose_rows += [poses[i].reshape((s[0]*s[1]*s[2],))]


            # print(joint_rows.shape, joint_cols.shape)
            joint_df = pd.DataFrame(joint_rows, columns=joint_cols)
            # pose_df = pd.DataFrame(pose_rows)
            print(json.dumps(NTU25_CONNECTIONS))

            for col in joint_df.columns:
                joint_df[col] = signal.savgol_filter(joint_df[col], 20, 2, mode='interp')

            joint_df.round(4).to_csv(os.path.join(dst, f'{folder}.csv'), index=False)
            # pose_df.round(4).to_csv(os.path.join(dst, f'{folder}_pose.csv'), index=False, sep=',')
