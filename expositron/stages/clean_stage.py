import json
import shutil
import os.path as osp
import numpy as np
import pandas as pd
from alive_progress import alive_bar
from expositron.utils import *


def push_folders(src):
    csv_files = list_files(src, 'csv')
    names = []
    for file, csv_file in csv_files:
        names += [file.split('.')[0]]
    with alive_bar(len(list_folders(src)), theme='classic', title='Cleaning') as bar:
        for folder, src_folder in list_folders(src):
            bar()
            if folder in names:
                dst = f'_redo2/{src_folder}'
                shutil.copytree(src_folder, dst, symlinks=False, ignore=None, copy_function=shutil.copy2, ignore_dangling_symlinks=False, dirs_exist_ok=False)


def push_csv(src):
    csv_files = list_files(src, 'csv')
    with alive_bar(len(csv_files), theme='classic', title='Pushing bad csv') as bar:
        for file, csv_file in csv_files:
            bar()
            if len(open(csv_file, 'r').readlines()) < 120:
                print(csv_file)
                shutil.move(csv_file, '.')


def rename_folders(src):
    src_folders = list_folders(src)
    count = {}
    with alive_bar(len(src_folders), theme='classic', title='Cleaning') as bar:
        for folder, src_folder in list_folders(src):
            bar()
            p, g, s, t, c = split(folder, 4)
            p, g, s, t, c = p[0]+p[2]+p[3], g[0]+g[2]+g[3], s[0]+s[2]+s[3], t[0]+t[2]+t[3], c[0]+c[2]+c[3]
            folder_ = p + g + s + t + c
            os.rename(src_folder, osp.join(src, folder_))


def rename_csv(src):
    csv_files = list_files(src, 'csv')
    with alive_bar(len(csv_files), theme='classic', title='Cleaning') as bar:
        for file, csv_file in csv_files:
            bar()
            pref = csv_file.split(osp.sep)[0].split('-')[0].replace('G', 'P')
            csv_file_new = csv_file.replace('P001', pref)
            os.rename(csv_file, csv_file_new)


def clean_stage(src):
    rename_folders(src)
    append(src, 'cleaned', 'success')
