import csv
import cv2
import json
import shutil
import os
import os.path as osp
import mediapipe as mp
from alive_progress import alive_bar

mp_drawing = mp.solutions.drawing_utils
mp_drawing_styles = mp.solutions.drawing_styles
mph = mp.solutions.holistic


def list_files(src, ext=None):
    return [(f.name, f.path) for f in sorted(os.scandir(src), key=lambda f: f.name) if f.is_file() and f.name.endswith(ext)]


def list_folders(src):
    return [(f.name, f.path) for f in sorted(os.scandir(src), key=lambda f: f.name) if f.is_dir()]


def media_stage(src, dst):
    src_folders = list_folders(src)

    #if osp.exists(dst): shutil.rmtree(src)
    os.makedirs(dst, exist_ok=True)

    bones = [[a, b] for a, b in mph.POSE_CONNECTIONS]
    with open(osp.join(dst, 'bones.txt'), 'w') as out:
        json.dump(bones, out, indent=True)

    holistic = mph.Holistic(
        static_image_mode=False,
        model_complexity=2,
        smooth_landmarks=True,
        enable_segmentation=True,
        smooth_segmentation=True,
        refine_face_landmarks=False
    )

    with alive_bar(len(src_folders), theme='classic', title='Parsing') as bar:
        for folder, src_folder in src_folders:
            bar()
            joints = []
            dst_file = osp.join(dst, f'{folder}.csv')
            for file, src_file in list_files(src_folder, 'jpg'):
                image = cv2.cvtColor(cv2.imread(src_file), cv2.COLOR_BGR2RGB)
                height, width, _ = image.shape
                result = holistic.process(image)
                joint = []
                if result.pose_world_landmarks:
                    for it in mph.PoseLandmark:
                        pw = result.pose_world_landmarks.landmark[it]
                        if pw:
                            joint += [pw.x, pw.y, pw.z]
                        else:
                            joint += [None, None, None]
                else:
                    joint += [None] * 33
                if result.left_hand_landmarks:
                    for it in mph.HandLandmark:
                        lh = result.left_hand_landmarks.landmark[it]
                        if lh:
                            joint += [lh.x, lh.y, lh.z]
                        else:
                            joint += [None, None, None]
                else:
                    joint += [None] * 21
                if result.right_hand_landmarks:
                    for it in mph.HandLandmark:
                        rh = result.right_hand_landmarks.landmark[it]
                        if rh:
                            joint += [rh.x, rh.y, rh.z]
                        else:
                            joint += [None, None, None]
                else:
                    joint += [None] * 21
                joints += [joint]

            if joints:
                head = []
                for it in mph.PoseLandmark:
                    head += [f'joint__body__{it.name.lower()}__x']
                    head += [f'joint__body__{it.name.lower()}__y']
                    head += [f'joint__body__{it.name.lower()}__z']
                for it in mph.HandLandmark:
                    head += [f'joint__left_hand__{it.name.lower()}__x']
                    head += [f'joint__left_hand__{it.name.lower()}__y']
                    head += [f'joint__left_hand__{it.name.lower()}__z']
                for it in mph.HandLandmark:
                    head += [f'joint__right_hand__{it.name.lower()}__x']
                    head += [f'joint__right_hand__{it.name.lower()}__y']
                    head += [f'joint__right_hand__{it.name.lower()}__z']

                with open(dst_file, 'w', encoding='UTF8') as out:
                    writer = csv.writer(out)
                    writer.writerow(head)
                    for joint in joints:
                        writer.writerow(joint)

                joints_file = os.path.join(dst, 'joints.txt')
                with open(joints_file, 'w', encoding='UTF8') as out:
                    out.write('\n'.join(head))
