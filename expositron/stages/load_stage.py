import os
import json
import shutil
from alive_progress import alive_bar
from expositron.utils import *


def load_stage(src, dst):
    src_folders = list_folders(src)
    os.makedirs(dst, exist_ok=True)
    classes = open(osp.join(src, 'classes.txt'), 'a')

    count = 0
    with alive_bar(len(src_folders), theme='classic') as bar:
        for pid, (person, person_folder) in enumerate(src_folders):
            bar()
            for gid, (gesture, gesture_folder) in enumerate(list_folders(person_folder)):
                for sid, (side, side_folder) in enumerate(list_folders(gesture_folder)):
                    class_ = f'{person}_{gesture}_{side}'
                    for tid, (trial, trial_folder) in enumerate(list_folders(side_folder)):
                        for cid, (camera, camera_folder) in enumerate(list_folders(trial_folder)):
                            folder = f'P{str(pid+1).zfill(2)}G{str(gid+1).zfill(2)}S{str(sid+1).zfill(2)}T{str(tid+1).zfill(2)}C{str(cid+1).zfill(2)}'
                            dst_folder = osp.join(dst, folder)
                            count += 1
                            info_file = osp.join(trial_folder, 'info.json')
                            if os.path.exists(info_file):
                                with open(info_file, 'r') as inp:
                                    info = json.loads(inp.read())
                                    if 'label' in info:
                                        is_valid = info['label']['is_valid_performance']
                            else:
                                is_valid = False
                            if is_valid:
                                os.makedirs(dst_folder, exist_ok=True)
                                for channel, channel_folder in list_folders(camera_folder):
                                    for i, (_, channel_file) in enumerate(list_files(channel_folder, 'jpg')):
                                        dst_file = osp.join(dst_folder, f'{channel}{str(i+1).zfill(3)}.jpg')
                                        shutil.copyfile(channel_file, dst_file)
    append(dst, 'loaded', 'success')
    print(count)
